# 1 run json server
npm run json-server
# 2 run app
npm start
# Structure Project
● src
+---● app
|   |
|   +--● shared
|   |  |
|   |  +--● core
|   |  |  |--base-api.service.ts
|   |  |
|   |  +--● models
|   |  |  |--book.model.ts
|   |  |  |--category-type.model.ts
|   |  |  |--category.model.ts
|   |  |  |--image.model.ts
|   |  |
|   |  +--● pipes
|   |  |  |--search.pipe.ts
|   |  |  |--valuearray.pipe.ts
|   |  |
|   |  +--● services
|   |  |  |--book.service.ts
|   |  |  |--category-type.service.ts
|   |  |  |--category.service.ts
|   |  |  |--image.service.ts
|   |  |  |--pager.service.ts
|   |  |--shared.module.ts
|   |  |
|   +--● view
|   |  |
|   |  +--● components
|   |  |  |
|   |  |  |+--● book-details
|   |  |  |   |--book-details.component.ts|.html
|   |  |  |
|   |  |  |+--● category
|   |  |  |   |--category.component.ts|.html
|   |  |  |
|   |  |  |+--● category-type
|   |  |  |   |--category-type.component.ts|.html
|   |  |  |
|   |  |  |+--● home
|   |  |  |   |--home.component.ts|.html
|   |  |  |
|   |  |  |+--● openlibrary
|   |  |  |   |--openlibrary.component.ts|.html
|   |  |  |
|   |  |  |+--● page-not-found
|   |  |  |   |--page-not-found.component.ts|.html
|   |  |  |
|   |  |
|   |  +--● containers
|   |  |  |
|   |  |  |+--● nav-menu
|   |  |  |   |--nav-menu.container.ts|.html
|   |  |  |
|   |--app-routing.module.ts
|   |
|   |--app.component.ts|.html
|   |
|   |--app.module.ts
