import { Component, ViewChild, ElementRef , Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { BookService } from '../../../shared/services/book.service';
import { ImageService } from '../../../shared/services/images.service';
import { CategoryService } from '../../../shared/services/category.service';
import { CategoryTypeService } from '../../../shared/services/category-type.service';

import { ImageModel } from '../../../shared/models/image.model';
import { Book } from '../../../shared/models/book.model';
import { Category } from '../../../shared/models/category.model';
import { CategoryType } from '../../../shared/models/category-type.model';

@Component({
    selector: 'book-details',
    providers: [BookService],
    templateUrl: './book-details.component.html'
})
export class BookDetailComponent implements OnInit {
    @Input() public selectBook: Book
    @ViewChild('bookItemName') bookItemName;
    @ViewChild('bookItemAuthor') bookItemAuthor;
    @ViewChild('bookItemSection') bookItemSection;
    @ViewChild('bookItemSectionRow') bookItemSectionRow;
    @ViewChild('bookItemYearCreate') bookItemYearCreate;
    @ViewChild('bookItemYearPrint') bookItemYearPrint;
    @ViewChild('bookItemTags') bookItemTags;

    public editFild:boolean = false;
    public saveImage: ImageModel = null;
    public viewImage: ImageModel = null;
    public file:File = null;

    constructor(
        private bookService: BookService,
        private imageService: ImageService,
        private categoryService: CategoryService
    ) { }
    public ngOnInit(): void {
        if(this.selectBook.image){
            this.imageService.getImage(this.selectBook.image).subscribe((data: ImageModel)=>{
               this.viewImage = data;
            });
        }
    }    
    public toggleEdit(cancel:boolean=true){
        this.editFild = !this.editFild;
        if(!cancel) return;
    }
    public toggleSave(index: number) {
        const editBook:Book = {
            name: this.bookItemName.nativeElement.value,
            author: this.bookItemAuthor.nativeElement.value,
            categorys: this.selectBook.categorys,
            section: this.bookItemSection.nativeElement.value,
            sectionrow: this.bookItemSectionRow.nativeElement.value,
            yearcreate: this.bookItemYearCreate.nativeElement.value,
            yearprint: this.bookItemYearPrint.nativeElement.value,
            tags: this.bookItemTags.nativeElement.value,
            id: this.selectBook.id
        };
        this.bookService.editBook(editBook).subscribe((data: CategoryType) => {
            this.selectBook = editBook;
            this.editFild = false;
        });    
    }
    public changeFile($event) : void {
        this.uploadFileOnServer($event.target);
    }
    public uploadFileOnServer(inputValue: any): void {
        var file:File = inputValue.files[0];
        var imgReader:FileReader = new FileReader();
        imgReader.onloadend = (e) => {
            this.saveImage = {
                bookid: this.selectBook.id,
                src: imgReader.result
            };
            if(this.viewImage) {
                this.imageService.deleteImage(this.viewImage.id).subscribe((data: ImageModel)=>{
                    this.imageService.saveImage(this.saveImage).subscribe((data: ImageModel)=>{
                        this.selectBook.image = data.id;
                        this.viewImage = data;
                        this.bookService.editBook(this.selectBook).subscribe((data: CategoryType) => { });
                    });
                });
                return;
            }
            this.imageService.saveImage(this.saveImage).subscribe((data: ImageModel)=>{
                this.selectBook.image = data.id;
                this.viewImage = data;
                this.bookService.editBook(this.selectBook).subscribe((data: CategoryType) => { });
            });
            
        }
        imgReader.readAsDataURL(file);
    }
}