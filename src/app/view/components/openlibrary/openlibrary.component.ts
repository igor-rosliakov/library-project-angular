import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';

import { BookService } from '../../../shared/services/book.service';
import { CategoryService } from '../../../shared/services/category.service';
import { CategoryTypeService } from '../../../shared/services/category-type.service';
import { PagerService } from '../../../shared/services/pager.service'

import { Book } from '../../../shared/models/book.model';
import { Category } from '../../../shared/models/category.model';
import { CategoryType } from '../../../shared/models/category-type.model';
@Component({
  selector: 'openlibrary',
  templateUrl: './openlibrary.component.html'
})
export class OpenLibraryComponent implements OnInit {
    @ViewChild('bookEditRow') bookEditRow;
    @ViewChild('bookItemName') bookItemName;
    @ViewChild('bookItemAuthor') bookItemAuthor;
    @ViewChild('bookItemSection') bookItemSection;
    @ViewChild('bookItemRow') bookItemRow;
    
    public bookData: Book[] = null;
    public categoryTypeData: CategoryType[] = null;
    public categoryData: Category[] = null;
    public selectedComboboxValue: Category[] = null;
    public addNewItem = false;
    public editRow = false;

    public pager: any = {};
    public pagedItems: any[];
    public newAttribute: Book = {author:'', categorys:'',section:'',sectionrow:'',name:''};
    public NewCategoryType:number = 0;
    public displayingIndeces: boolean[];
    public displayBookingIndeces: boolean[];
    public orderParam: string="";
    public orderParamType:boolean = null;
    public dropdownList = [];
    public selectedItems = [];
    public dropdownSettings = {};

    constructor(
        private bookService: BookService,
        private pagerService: PagerService,
        private categoryService: CategoryService,
        private categoryTypeService: CategoryTypeService
      ) { }
  
    ngOnInit() {
        this.getAllCategorys();
        this.getAllBooks();     
        this.dropdownSettings = { 
            singleSelection: false, 
            text:"Select categorys",
            selectAllText:'Select All',
            unSelectAllText:'UnSelect All',
            enableSearchFilter: true,
            classes:"myclass custom-class"
        }; 
    }
    public getAllCategorys(){
        this.categoryService.getCategorys()
         .subscribe((data: Category[]) => {
            
            this.categoryData = data;
            this.categoryData.forEach(el=>{
                this.dropdownList.push({id:el.id,itemName:el.name});
            });
        }); 
    }
    public getAllBooks(){
        this.bookService.getBooks()
        .subscribe((data: Book[]) => {
            this.bookData = data;   
            this.setCategoryObject();  
            this.displayingIndeces = new Array(data.length);
            this.displayingIndeces.fill(false);
            this.displayBookingIndeces = new Array(data.length);
            this.displayBookingIndeces.fill(false);
            this.setPage(1);    
        }); 
    }
    public setCategoryObject(){
        this.bookData.forEach(itemBook => {
            if(itemBook.categorys){
                itemBook.categorys.forEach(itemcategory =>{
                    const index: number = itemBook.categorys.indexOf(itemcategory); 
                    itemcategory = this.categoryData.find(getdatacategory => getdatacategory.id == itemcategory.id);
                    itemBook.categorys[index]= itemcategory;
                });
            }
        });
    }
    public setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }
        this.pager = this.pagerService.getPager(this.bookData.length, page);
        this.pagedItems = this.bookData.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }
    public iconOrderBy(paramName:string,paramType:boolean){
        if(paramName==this.orderParam && paramType==this.orderParamType) return 1;
    }
    public orderBy(param:string){
        this.orderParam = param;
        if(this.orderParamType==null) this.orderParamType = false;
        this.orderParamType =! this.orderParamType;
        if(param=="name" && this.orderParamType)this.bookData = this.bookData.sort(this.startOrderByNameAsk);
        if(param=="name" && !this.orderParamType)this.bookData = this.bookData.sort(this.startOrderByNameDesk);
        if(param=="author" && this.orderParamType)this.bookData = this.bookData.sort(this.startOrderByAuthorAsk);
        if(param=="author" && !this.orderParamType)this.bookData = this.bookData.sort(this.startOrderByAuthorDesk);
        if(param=="section" && this.orderParamType)this.bookData = this.bookData.sort(this.startOrderBySectionAsk);
        if(param=="section" && !this.orderParamType)this.bookData = this.bookData.sort(this.startOrderBySectionDesk);
        if(param=="row" && this.orderParamType)this.bookData = this.bookData.sort(this.startOrderBySectionRowAsk);
        if(param=="row" && !this.orderParamType)this.bookData = this.bookData.sort(this.startOrderBySectionRowDesk);
        this.setPage(1);
    }
    public startOrderByNameAsk(c1: Book,c2:Book){
        if(c1.name > c2.name) return 1;
        if(c1.name === c2.name) return 0;
        if(c1.name < c2.name) return -1;
    }
    public startOrderByNameDesk(c1: Book,c2:Book){
        if(c1.name < c2.name) return 1;
        if(c1.name === c2.name) return 0;
        if(c1.name > c2.name) return -1;
    }
    public startOrderByAuthorAsk(c1: Book,c2:Book){
        if(c1.name > c2.name) return 1;
        if(c1.name === c2.name) return 0;
        if(c1.name < c2.name) return -1;
    }
    public startOrderByAuthorDesk(c1: Book,c2:Book){
        if(c1.author < c2.author) return 1;
        if(c1.author === c2.author) return 0;
        if(c1.author > c2.author) return -1;
    }
    public startOrderBySectionAsk(c1: Book,c2:Book){
        if(c1.section > c2.section) return 1;
        if(c1.section === c2.section) return 0;
        if(c1.section < c2.section) return -1;
    }
    public startOrderBySectionDesk(c1: Book,c2:Book){
        if(c1.section < c2.section) return 1;
        if(c1.section === c2.section) return 0;
        if(c1.section > c2.section) return -1;
    }
    public startOrderBySectionRowAsk(c1: Book,c2:Book){
        if(c1.sectionrow > c2.sectionrow) return 1;
        if(c1.sectionrow === c2.sectionrow) return 0;
        if(c1.sectionrow < c2.sectionrow) return -1;
    }
    public startOrderBySectionRowDesk(c1: Book,c2:Book){
        if(c1.sectionrow < c2.sectionrow) return 1;
        if(c1.sectionrow === c2.sectionrow) return 0;
        if(c1.sectionrow > c2.sectionrow) return -1;
    }
    public showEditButton(){
        this.editRow = ! this.editRow
    }
    public addFormInput(){
        this.addNewItem = !this.addNewItem;
    }
    public cancelFieldValue(){
        this.addNewItem = !this.addNewItem;
        this.newAttribute = {author:'', categorys:'',section:'',sectionrow:'',name:''};
    }
    public isDisplay(index:number) {
        return this.displayingIndeces[index];
    }
    public isDisplayBook(index:number) {
        return this.displayBookingIndeces[index];
    }
    public toggleShowFullBook(index: number){
        this.displayBookingIndeces[index] = !this.displayBookingIndeces[index];
    }
    public toggleAdd() {
        this.addNewItem = !this.addNewItem;
        this.newAttribute.categorys = this.selectedItems.map(function(el){
            return {id:el.id};
        });
        this.bookService.saveBook(this.newAttribute).subscribe((data: CategoryType) => {
            this.newAttribute.categorys.forEach(el=>{
                const index: number = this.newAttribute.categorys.indexOf(el); 
                el = this.categoryData.find(getdatacategory => getdatacategory.id == el.id);
                this.newAttribute.categorys[index]= el;
            })
            this.newAttribute.id = data.id;
            this.bookData.push(this.newAttribute);
            this.newAttribute = {
                name:'',
                author:'',
                categorys:'',
                section:'',
                sectionrow:''
            };
            this.selectedItems = null;
            this.setPage(1);
        });
    }
    public toggleEdit(index: number, cancel:boolean = true) {
        this.selectedItems = [];
        const indexSelect: number = index + this.pager.startIndex; 
        this.bookData[indexSelect].categorys.forEach(el=>{
            this.selectedItems.push({id:el.id,itemName:el.name});
        });
        this.displayingIndeces = [false];
        if(!cancel) return;
        this.displayingIndeces[index] = !this.displayingIndeces[index];
    }
    public toggleSave(index: number) {
        const editBook:Book = {
            name: this.bookItemName.nativeElement.value,
            author: this.bookItemAuthor.nativeElement.value,
            categorys: this.selectedItems.map(function(el){return {id:el.id};}),
            section: this.bookItemSection.nativeElement.value,
            sectionrow: this.bookItemRow.nativeElement.value,
            id: this.bookData[index+this.pager.startIndex].id
        };
        this.bookService.editBook(editBook).subscribe((data: CategoryType) => {
            this.bookData[index+this.pager.startIndex] = editBook;
            this.setPage(this.pager.currentPage);
            this.setCategoryObject();
            this.displayingIndeces = [false];
        });     
    }
    public toggleDelete(index: number) {
        if (!confirm("Are you sure you want to remove that?"))  return;
        const indexDel: number = index + this.pager.startIndex; 
        this.bookService.deleteBook(this.bookData[indexDel]).subscribe((data: CategoryType) => {
            this.bookData.splice(indexDel, 1);
            this.setPage(this.pager.currentPage);
        }); 
    }
}

