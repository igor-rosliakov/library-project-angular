import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { FormGroup, FormControl, Validators } from '@angular/forms';

import { PagerService } from '../../../shared/services/pager.service'
import { CategoryService } from '../../../shared/services/category.service';
import { CategoryTypeService } from '../../../shared/services/category-type.service';
import { Category } from '../../../shared/models/category.model';
import { CategoryType } from '../../../shared/models/category-type.model';
@Component({
  selector: 'category-type',
  templateUrl: './category-type.component.html'
})
export class CategoryTypeComponent implements OnInit {
  @ViewChild('catEditRow') catEditRow;
  @ViewChild('catName') catName;

  public subscription: Subscription;
  public categoryTypeData: CategoryType[] = null;
  public gridData: Category[] = null;

  public addNewItem = false;
  public editRow = false;
  public pager: any = {};

  public pagedItems: any[];
  public newAttribute: CategoryType = {name:''};
  public NewCategoryType:number = 0;
  public displayingIndeces: boolean[];
  public orderParam: string="";
  public orderParamType:boolean = null;

  constructor(
    private categoryService: CategoryService,
    private pagerService: PagerService,
    private categoryTypeService: CategoryTypeService
  ) { }
  ngOnInit() {
    this.getAllCategorysType();
  }
  public getAllCategorysType() {
    this.categoryTypeService.getCategorysType()
      .subscribe((data: CategoryType[]) => {
        this.categoryTypeData = data;
        this.displayingIndeces = new Array(data.length);
        this.displayingIndeces.fill(false);
        this.setPage(1);
    });
  }
  public setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) {
        return;
    }
    this.pager = this.pagerService.getPager(this.categoryTypeData.length, page);
    this.pagedItems = this.categoryTypeData.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
  public orderBy(param:string){
    this.orderParam = param;
    if(this.orderParamType==null) this.orderParamType = false;
    this.orderParamType =! this.orderParamType;
    if(param=="name" && this.orderParamType)this.categoryTypeData = this.categoryTypeData.sort(this.startOrderByNameAsk);
    if(param=="name" && !this.orderParamType)this.categoryTypeData = this.categoryTypeData.sort(this.startOrderByNameDesk);
    this.setPage(1);
  }
  public iconOrderBy(paramName:string,paramType:boolean){
    if(paramName==this.orderParam && paramType==this.orderParamType) return 1;
  }
  public startOrderByNameAsk(c1: Category,c2:Category){
    if(c1.name > c2.name) return 1;
    if(c1.name === c2.name) return 0;
    if(c1.name < c2.name) return -1;
  }
  public startOrderByNameDesk(c1: Category,c2:Category){
    if(c1.name < c2.name) return 1;
    if(c1.name === c2.name) return 0;
    if(c1.name > c2.name) return -1;
  }
  public isDisplay(index:number) {
    return this.displayingIndeces[index];
  }
  public showEditButton(){
    this.editRow = ! this.editRow
  }
  public addFormInput(){
    this.addNewItem = !this.addNewItem;
  }
  public cancelFieldValue(){
    this.addNewItem = !this.addNewItem;
    this.newAttribute = {name:''};
  }
  public toggleAdd() {
    this.addNewItem = !this.addNewItem;
    this.categoryTypeService.saveCategoryType(this.newAttribute).subscribe((data: CategoryType) => {
      this.categoryTypeData.push(this.newAttribute);
      this.newAttribute = null;
      this.setPage(1);
    });
  }
  public toggleEdit(index: number, cancel:boolean = true) {
    this.displayingIndeces = [false];
    if(!cancel) return;
    this.displayingIndeces[index] = !this.displayingIndeces[index];
  }
  public toggleSave(index: number) {
    const editCat:CategoryType = {
      name:this.catName.nativeElement.value,
      id: this.categoryTypeData[index+this.pager.startIndex].id
    };
    this.displayingIndeces = [false];
    this.categoryTypeService.editCategoryType(editCat).subscribe((data: CategoryType) => {
      this.categoryTypeData[index+this.pager.startIndex] = editCat;
      this.catName = null;
      this.setPage(this.pager.currentPage);
    });
  }
  public toggleDelete(index: number) {
    if (!confirm("Are you sure you want to remove that?")) return;
    const indexDel: number = index + this.pager.startIndex; 
    this.categoryTypeService.deleteCategoryType(this.categoryTypeData[indexDel]).subscribe((data: CategoryType) => {
      this.categoryTypeData.splice(indexDel, 1);
      this.setPage(this.pager.currentPage);
    });
    
  }
}
