import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { FormGroup, FormControl, Validators } from '@angular/forms';

import { PagerService } from '../../../shared/services/pager.service'
import { CategoryService } from '../../../shared/services/category.service';
import { CategoryTypeService } from '../../../shared/services/category-type.service';
import { Category } from '../../../shared/models/category.model';
import { CategoryType } from '../../../shared/models/category-type.model';

@Component({
  selector: 'category',
  templateUrl: './category.component.html'
})
export class CategoryComponent implements OnInit {
  @ViewChild('catEditRow') catEditRow;
  @ViewChild('catName') catName;
  @ViewChild('selectType') selectType;

  subscription: Subscription;

  public categoryTypeData: CategoryType[] = null;
  public gridData: Category[] = null;
  public addNewItem = false;
  public editRow = false;
  public pager: any = {};
  public pagedItems: any[];
  public newAttribute: Category = {name:'',type:''};
  public NewCategoryType:number = 0;
  public displayingIndeces: boolean[];
  public orderParam: string="";
  public orderParamType:boolean = null;

  constructor(
    private categoryService: CategoryService,
    private pagerService: PagerService,
    private categoryTypeService: CategoryTypeService
  ) { }

  ngOnInit() {
    this.getAllCategorysType();
    this.getAllCategorys();
  }
  public getAllCategorysType() {
    this.categoryTypeService.getCategorysType()
      .subscribe((data: CategoryType[]) => {
        this.categoryTypeData = data;
        this.displayingIndeces = new Array(data.length);
        this.displayingIndeces.fill(false);
      });
  }
  public getAllCategorys() {
    this.categoryService.getCategorys()
      .subscribe((data: Category[]) => {
        this.gridData = data;
        this.gridData.forEach(item => {
          const ct = this.categoryTypeData.find(x => x.id == item.type);
          item.type = ct ? ct : undefined;
        });
        this.setPage(1);
      });
  }
  public setPage(page: number) {
    if (page < 1 || page > this.pager.totalPages) return;
    this.pager = this.pagerService.getPager(this.gridData.length, page);
    this.pagedItems = this.gridData.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }
  public iconOrderBy(paramName:string,paramType:boolean){
    if(paramName==this.orderParam && paramType==this.orderParamType) return 1;
  }
  public setSelectNewCategoryType(id: any): void {
    this.NewCategoryType = id;
  }
  public setSelectCategoryType(id: any): void {
    this.newAttribute.type = id;
  }
  public startOrderByNameAsk(c1: Category,c2:Category){
    if(c1.name > c2.name) return 1;
    if(c1.name === c2.name) return 0;
    if(c1.name < c2.name) return -1;
  }
  public startOrderByNameDesk(c1: Category,c2:Category){
    if(c1.name < c2.name) return 1;
    if(c1.name === c2.name) return 0;
    if(c1.name > c2.name) return -1;
  }
  public startOrderByTypeAsk(c1: Category,c2:Category){
    if(c1.type){
      if(c1.type.name > c2.type.name) return 1;
      if(c1.type.name === c2.type.name) return 0;
      if(c1.type.name < c2.type.name) return -1;
    }
  }
  public startOrderByTypeDesk(c1: Category,c2:Category){
    if(c1.type){
      if(c1.type.name < c2.type.name) return 1;
      if(c1.type.name === c2.type.name) return 0;
      if(c1.type.name > c2.type.name) return -1;
    }
  }
  public orderBy(param:string){
    this.orderParam = param;
    if(this.orderParamType==null) this.orderParamType = false;
    this.orderParamType =! this.orderParamType;
    if(param=="name" && this.orderParamType)this.gridData = this.gridData.sort(this.startOrderByNameAsk);
    if(param=="name" && !this.orderParamType)this.gridData = this.gridData.sort(this.startOrderByNameDesk);
    if(param=="type" && this.orderParamType)this.gridData = this.gridData.sort(this.startOrderByTypeAsk);
    if(param=="type" && !this.orderParamType)this.gridData = this.gridData.sort(this.startOrderByTypeDesk);
    this.setPage(1);
  }
  public showEditButton(){
    this.editRow = ! this.editRow
  }
  public addFormInput(){
    this.addNewItem = !this.addNewItem;
  }
  
  public cancelFieldValue(){
    this.addNewItem = !this.addNewItem;
    this.newAttribute = {name:'',type:''};
  }
  public isDisplay(index:number) {
    return this.displayingIndeces[index];
  }
  public toggleAdd() {
    this.addNewItem = !this.addNewItem;
    this.newAttribute.type = parseInt(this.newAttribute.type);
    this.categoryService.saveCategory(this.newAttribute).subscribe((data: CategoryType) => {
      this.newAttribute.type = this.categoryTypeData.find(x => x.id === this.newAttribute.type);
      this.newAttribute.id = data.id;
      this.gridData.push(this.newAttribute);
      this.newAttribute = null;
      this.setPage(1);
    });
  }
  public toggleEdit(index: number, cancel:boolean = true) {
    this.displayingIndeces = [false];
    if(!cancel) return;
    this.displayingIndeces[index] = !this.displayingIndeces[index];
  }
  public toggleSave(index: number) {
    const editCat:Category = {
      name:this.catName.nativeElement.value,
      type:parseInt(this.selectType.nativeElement.value),
      id: this.gridData[index+this.pager.startIndex].id
    };
    this.categoryService.editCategory(editCat).subscribe((data: CategoryType) => {
      this.gridData[index+this.pager.startIndex] = editCat;
      this.gridData[index+this.pager.startIndex].type = this.categoryTypeData.find(x => x.id === editCat.type);
      this.displayingIndeces = [false];
      this.catName = null;
      this.selectType = null;
      this.setPage(this.pager.currentPage);
    });
  }
  public toggleDelete(index: number) {
    if (!confirm("Are you sure you want to remove that?")) return;
    const indexDel: number = index + this.pager.startIndex; 
    this.categoryService.deleteCategory(this.gridData[indexDel]).subscribe((data: CategoryType) => {
      this.gridData.splice(indexDel, 1);
      this.setPage(this.pager.currentPage);
    });
  }
}
