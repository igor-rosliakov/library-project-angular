import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
  name: 'searchpipe'
})
export class SearchPipe implements PipeTransform {
    transform(items: any[], namefild: string, authorfild?: any, bookpage?:any): any[] {
        if(!items) return [];
        if(!namefild && authorfild==0) {
            return items;
        }
        if(namefild && bookpage==undefined){
            namefild = namefild.toLowerCase();
            return items.filter( it => {
                return it.name.toLowerCase().includes(namefild);
            });
        }
        if(bookpage){
            if(namefild){
                if(authorfild){
                    namefild = namefild.toLowerCase();
                    authorfild = authorfild.toLowerCase();        
                    return items.filter( it => {
                        return it.name.toLowerCase().includes(namefild) && it.author.toLowerCase().includes(authorfild);
                    });
                }

                namefild = namefild.toLowerCase();    
                return items.filter( it => {
                    return it.name.toLowerCase().includes(namefild);
                });
            }
            if(authorfild){
                authorfild = authorfild.toLowerCase();    
                return items.filter( it => {
                    return it.author.toLowerCase().includes(authorfild);
                });
            }
            return items;
        }
    }   
}