import { Pipe, PipeTransform } from '@angular/core';
@Pipe({
    name: 'valuearray'
})
export class ValueArrayPipe implements PipeTransform {
  transform(value: any, args?: any): any {
    return value.map(el=>el.name);
  }
}