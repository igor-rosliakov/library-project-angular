export class CategoryType {
  constructor(
    public name: string,
    public id?: number
  ) {}
}