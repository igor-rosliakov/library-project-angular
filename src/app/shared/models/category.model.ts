import { CategoryType } from "./category-type.model";

export class Category {
    constructor(
      public name: string,
      public type: any,
      public id?: number
    ) {}
  }