export class Book {
  constructor(
    public name: string,
    public author: string,
    public categorys: any,
    public section: string,
    public sectionrow: string,
    public pagecount?: number,
    public image?: number,
    public yearcreate?: string,
    public yearprint?: string,
    public tags?: any,
    public id?: number
  ) {}
}