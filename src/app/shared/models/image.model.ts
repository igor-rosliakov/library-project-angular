export class ImageModel {
  constructor(
    public src: any,
    public bookid: number,
    public id?: number
  ) {}
}