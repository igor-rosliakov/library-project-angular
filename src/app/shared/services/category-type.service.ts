import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { BaseApiService } from '../core/base-api.service';
import { CategoryType } from '../models/category-type.model';

@Injectable()
export class CategoryTypeService extends BaseApiService {
  constructor(public http: Http) {
    super(http);
  }
  getCategorysType():  Observable<CategoryType[]> {
    return this.get("categoriesType");
  }
  saveCategoryType(categoryType: CategoryType):  Observable<CategoryType> {
    return this.post("categoriesType", categoryType)
  }
  editCategoryType(categoryType: CategoryType) {
     return this.put("categoriesType/"+categoryType.id, categoryType)
  }
  deleteCategoryType(categoryId: CategoryType) {
     return this.delete("categoriesType/"+categoryId.id)
  }
}
