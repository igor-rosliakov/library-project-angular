import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { BaseApiService } from '../core/base-api.service';
import { ImageModel } from '../models/image.model';

@Injectable()
export class ImageService extends BaseApiService {
  constructor(public http: Http) {
    super(http);
  }
  getImage(image: any):  Observable<ImageModel> {
    return this.get("images/"+image)
  }
  saveImage(image: any):  Observable<ImageModel> {
    return this.post("images", image)
  }
  editImage(image: ImageModel) {
    return this.put("images/"+image.id, image)
  }
  deleteImage(imageId: number):  Observable<ImageModel> {
    return this.delete("images/"+imageId)
  }
}
