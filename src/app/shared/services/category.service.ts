import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { BaseApiService } from '../core/base-api.service';
import { Category } from '../models/category.model';

@Injectable()
export class CategoryService extends BaseApiService {
  constructor(public http: Http) {
    super(http);
  }
  getCategorys():  Observable<Category[]> {
    return this.http.get("http://localhost:3004/categories").map((res: Response) => res.json())
  }
  saveCategory(category: Category):  Observable<Category> {
    return this.post("categories", category)
  }
  editCategory(category: Category):  Observable<Category> {
    return this.put("categories/"+category.id, category)
  }
  deleteCategory(category: Category):  Observable<Category> {
     return this.delete("categories/"+category.id)
  }
}
