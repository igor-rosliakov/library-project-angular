import { Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { BaseApiService } from '../core/base-api.service';
import { Book } from '../models/book.model';

@Injectable()
export class BookService extends BaseApiService {
  constructor(public http: Http) {
    super(http);
  }
  getBooks():  Observable<Book[]> {
    return this.get("books")
  }
  getBook(book: Book):  Observable<Book[]> {
    return this.get("books/"+book.id)
  }  
  saveBook(book: Book):  Observable<Book> {
    return this.post("books", book)
  }
  editBook(book: Book) {
     return this.put("books/"+book.id, book)
  }
  deleteBook(book: Book) {
     return this.delete("books/"+book.id)
  }
}
