import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CategoryComponent } from './view/components/category/category.component';
import { HomeComponent } from './view/components/home/home.component';
import { PageNotFoundComponent } from './view/components/page-not-found/page-not-found.component';
import { CategoryTypeComponent } from './view/components/category-type/category-type.component';
import { OpenLibraryComponent } from './view/components/openlibrary/openlibrary.component';

const routes: Routes = [
  { path: '',   redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'categorys', component: CategoryComponent },
  { path: 'categorys-type', component: CategoryTypeComponent },
  { path: 'openlibrary', component: OpenLibraryComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
