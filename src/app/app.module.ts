import { BrowserModule } from '@angular/platform-browser';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import 'rxjs/add/operator/map';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { BookDetailComponent } from './view/components/book-details/book-details.component';
import { CategoryComponent } from './view/components/category/category.component';
import { CategoryTypeComponent } from './view/components/category-type/category-type.component';
import { HomeComponent } from './view/components/home/home.component';
import { NavMenuContainer } from './view/containers/nav-menu/nav-menu.container';
import { OpenLibraryComponent } from './view/components/openlibrary/openlibrary.component';
import { PageNotFoundComponent } from './view/components/page-not-found/page-not-found.component';

import { BookService } from './shared/services/book.service';
import { CategoryService } from './shared/services/category.service';
import { CategoryTypeService } from './shared/services/category-type.service';
import { ImageService } from './shared/services/images.service';
import { PagerService } from './shared/services/pager.service';

import { ValueArrayPipe } from './shared/pipes/valuearray.pipe';
import { SearchPipe } from './shared/pipes/search.pipe';

import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
@NgModule({
  declarations: [
    AppComponent,
    BookDetailComponent,
    CategoryComponent,
    CategoryTypeComponent, 
    HomeComponent,
    NavMenuContainer,
    
    OpenLibraryComponent,
    PageNotFoundComponent,

    SearchPipe,
    ValueArrayPipe
  ],
  imports: [
    AngularMultiSelectModule,
    AppRoutingModule,
    BrowserModule,
    HttpModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,   
  ],
  providers: [
    BookService,
    CategoryService,
    CategoryTypeService,
    ImageService,
    PagerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
